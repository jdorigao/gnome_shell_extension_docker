
import GLib from 'gi://GLib'
import Gio from 'gi://Gio'

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import DockerManager from './dockerManager.js';

/**
 * Docker commands
 * @type {Array.<{label: string, command: string}}
 */
export const docker_commands = {
	// Service
	s_start: {
		label: "Start docker",
		command: "start docker"
	},
	// Container commands
	c_start: {
		label: "Start",
		command: "start"
	},
	c_start_i: {
		label: "Start interactive",
		command: "start -i"
	},
	c_restart: {
		label: "Restart",
		command: "restart"
	},
	c_stop: {
		label: "Stop",
		command: "stop"
	},
	c_pause: {
		label: "Pause",
		command: "pause"
	},
	c_unpause: {
		label: "Unpause",
		command: "unpause"
	},
	c_rm: {
		label: "Remove",
		command: "rm"
	},
	c_exec: {
		label: "Exec Bash",
		command: "exec -it"
	},
	c_attach: {
		label: "Attach Terminal",
		command: "attach"
	},
	c_inspect: {
		label: "Inspect",
		command: "inspect"
	},
	c_logs: {
		label: "View logs",
		command: "logs --tail 1000 -f"
	},

	compose_up: {
		label: "Up",
		command: "up -d"
	},
	compose_stop: {
		label: "Stop",
		command: "stop"
	},
	compose_rm: {
		label: "Remove",
		command: "down"
	},

	// Img commands
	i_rm: {
		label: "Remove",
		command: "rmi -f"
	},
	i_run: {
		label: "Run",
		command: "run --rm -d"
	},
	i_run_i: {
		label: "Run interactive",
		command: "run --rm -it"
	},
	i_inspect: {
		label: "Inspect",
		command: "inspect"
	}
}

/**
 * Check if docker is installed
 * @return {Boolean}
 */
export function is_docker_installed() {
	return !!GLib.find_program_in_path('docker');
}

/**
 * Check if docker-compose is installed
 * @return {Boolean}
 */
export function is_docker_compose_installed() {
	return !!GLib.find_program_in_path('docker-compose');
}

/**
 * Check if docker service is running
 * @return {Boolean} whether docker service is running or not
 */
export async function is_docker_running() {
	let command = `systemctl is-active --quiet docker`;

	let [, c] = GLib.shell_parse_argv(command);

	let is_running = false;
	await exec_communicate(c)
		.then(() => { is_running = true })
		.catch((err) => { });

	return is_running;
}

/**
 * Get total number of containers running-
 * @return {Promise<string>}
 */
export async function get_containers_running() {
	let out = "";

	await exec_communicate("docker info --format '{{json .ContainersRunning}}'")
		.then((output) => { out = output })
		.catch(() => {
			out = "0";
		});

	return out;
}


const inspect_template = `docker inspect --format '{{json .}}'`;
const [, list_containers_command] = GLib.shell_parse_argv("docker ps -aq");
const [, inspect_command] = GLib.shell_parse_argv(inspect_template);

/**
 * Return containers name and status.
 * @return {Array.<{
 * id: String,
 * name: String,
 * state: String,
 * ip: String,
 * ip_prefix: String,
 * gateway: String,
 * ports: String,
 * compose_project: String,
 * compose_dir: String}>} every object represents a container
 */
export async function get_containers() {
	let out = "";
	await exec_communicate(list_containers_command)
		.then((output) => { out = output })
		.catch((err) => {
			Main.notifyError(`Docker ERROR`, err.toString());
		});

	if (!out.length) {
		return [];
	}

	let [, ids] = GLib.shell_parse_argv(out)
	await exec_communicate(inspect_command.concat(ids))
		.then((output) => { out = output })
		.catch((err) => {
			Main.notifyError(`Docker ERROR`, err.toString());
		});

	// Found containers
	return out
		.split("\n")
		.map(container => {
			container = JSON.parse(container);

			let ip, gateway, ip_prefix = "";
			if (container.NetworkSettings && container.NetworkSettings.Networks) {
				ip = container.NetworkSettings.Networks[Object.keys(container.NetworkSettings.Networks)[0]].IPAddress;
				ip_prefix = container.NetworkSettings.Networks[Object.keys(container.NetworkSettings.Networks)[0]].IPPrefixLen;
				gateway = container.NetworkSettings.Networks[Object.keys(container.NetworkSettings.Networks)[0]].Gateway;
			}

			return {
				id: container.Id,
				name: container.Name.slice(1),
				state: container.State.Status,
				ip: ip,
				ip_prefix: ip_prefix,
				gateway: gateway,
				ports: container.HostConfig.PortBindings,
				compose_project: container.Config.Labels['com.docker.compose.project'],
				compose_dir: container.Config.Labels['com.docker.compose.project.working_dir']
			};
		})
		.sort((a, b) => {
			if (a.name < b.name) return -1;
			if (a.name > b.name) return 1;
			// equal name
			return 0;
		});
}

const list_images_template = `docker images --filter dangling=false --format "{{.ID}};{{.Repository}};{{.Tag}}"`;
const [, list_images_command] = GLib.shell_parse_argv(list_images_template);

/**
 * Return containers name and status.
 * @return {Array.<{id: String, name: String}>} every object represents a container
 */
export async function get_images() {
	let out = "";
	await exec_communicate(list_images_command)
		.then((output) => { out = output })
		.catch((err) => {
			Main.notifyError(`Docker ERROR`, err.toString());
		});

	if (!out.length) {
		return [];
	}

	// Found containers
	return out
		.replaceAll("\"", "")
		.split("\n")
		.filter(element => element)
		.map(str => str.split(";"))
		.map(s => {
			return {
				id: s[0],
				name: `${s[1]}:${s[2]}`
			};
		})
		.sort((a, b) => {
			if (a.name < b.name) return -1;
			if (a.name > b.name) return 1;
			// equal name
			return 0;
		});

}

/**
 * Run docker command.
 * @param {docker_commands} command - name of command from the list docker_commands
 * @param {Object} item - container/image struct
 */
export function run_command(command, item) {
	const Settings = DockerManager.settings;
	let c = "";
	switch (command) {
		// TODO: Make text to be translated
		case docker_commands.s_start:
			c = `systemctl ${command.command}`;
			item = { name: "" };
			break;
		case docker_commands.c_exec:
			c = `${Settings.get_string('terminal')} 'docker ${command.command} ${item.id} bash; read -p "Press enter to exit..."'`;
			GLib.spawn_command_line_async(c);
			return;
		case docker_commands.c_attach:
			c = `${Settings.get_string('terminal')} 'docker ${command.command} ${item.id}; read -p "Press enter to exit..."'`;
			GLib.spawn_command_line_async(c);
			return;

		case docker_commands.c_start_i:
		case docker_commands.c_inspect:
		case docker_commands.c_logs:
		case docker_commands.i_inspect:
		case docker_commands.i_run_i:
			c = `${Settings.get_string('terminal')} 'docker ${command.command} ${item.id}; read -p "Press enter to exit..."'`;
			GLib.spawn_command_line_async(c);
			return;

		case docker_commands.compose_up:
		case docker_commands.compose_stop:
		case docker_commands.compose_rm:
			if (GLib.chdir(item.compose_dir) != 0) {
				return;
			}
			c = `docker-compose ${command.command}`;
			break;

		default:
			c = `docker ${command.command} ${item.id}`;
	}

	let proc = Gio.Subprocess.new(
		c.split(" "),
		Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE
	);

	proc.communicate_utf8_async(null, null, (proc, res) => {
		try {
			let [, , stderr] = proc.communicate_utf8_finish(res);

			if (!proc.get_successful()) {
				throw new Error(stderr);
			}

			Main.notify(`${command.label}`, item.name);
		} catch (e) {
			Main.notifyError('Docker ERROR', e.toString());
		}
	});
}

/**
 * Execute a command asynchronously and return the output from `stdout` on
 * success or throw an error with output from `stderr` on failure.
 *
 * If given, @input will be passed to `stdin` and @cancellable can be used to
 * stop the process before it finishes.
 *
 * @param {string[]} argv - a list of string arguments
 * @param {string} [input] - Input to write to `stdin` or %null to ignore
 * @param {Gio.Cancellable} [cancellable] - optional cancellable object
 * @returns {Promise<string>} - The process output
 */
export async function exec_communicate(argv, input = null, cancellable = null) {
	let cancelId = 0;
	let flags = (Gio.SubprocessFlags.STDOUT_PIPE |
		Gio.SubprocessFlags.STDERR_PIPE);

	if (input !== null)
		flags |= Gio.SubprocessFlags.STDIN_PIPE;

	let proc = new Gio.Subprocess({
		argv: argv,
		flags: flags
	});
	proc.init(cancellable);

	if (cancellable instanceof Gio.Cancellable) {
		cancelId = cancellable.connect(() => proc.force_exit());
	}

	return new Promise((resolve, reject) => {
		proc.communicate_utf8_async(input, null, (proc, res) => {
			try {
				let [, stdout, stderr] = proc.communicate_utf8_finish(res);
				let status = proc.get_exit_status();

				if (status !== 0) {
					throw new Gio.IOErrorEnum({
						code: Gio.io_error_from_errno(status),
						message: stderr ? stderr.trim() : GLib.strerror(status)
					});
				}

				resolve(stdout.trim());
			} catch (e) {
				reject(e);
			} finally {
				if (cancelId > 0) {
					cancellable.disconnect(cancelId);
				}
			}
		});
	});
}
