import GObject from 'gi://GObject';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

import * as Confirm_Dialog from './dialogs/confirm.js';
import * as ContainerMenu from './containerMenu.js';

import * as Docker from '../lib/docker.js';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';


export const Container_Menu = GObject.registerClass(
	class Container_Menu extends ContainerMenu.Container_Menu {
		_init(container) {
			super._init(container);

			// Set size of sub menu. !important
			this.menu.actor.style = `min-height: ${container.settings.get_int('submenu-text')}px;`;

			this.add_information();

			this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem("Docker"));

			switch (container.state) {
				case "running":
					this._shell = new PopupMenu.PopupMenuItem(_("Exec Bash"));
					this._shell.connect('activate', () => Docker.run_command(Docker.docker_commands.c_exec, container));
					this.menu.addMenuItem(this._shell);

					this._attach = new PopupMenu.PopupMenuItem(_("Attach Terminal"));
					this._attach.connect('activate', () => Docker.run_command(Docker.docker_commands.c_attach, container));
					this.menu.addMenuItem(this._attach);

					this._pause = new PopupMenu.PopupMenuItem(_("Pause"));
					this._pause.connect('activate', () => Docker.run_command(Docker.docker_commands.c_pause, container));
					this.menu.addMenuItem(this._pause);

					this._stop = new PopupMenu.PopupMenuItem(_("Stop"));
					this._stop.connect('activate', () => Docker.run_command(Docker.docker_commands.c_stop, container));
					this.menu.addMenuItem(this._stop);

					this._restart = new PopupMenu.PopupMenuItem(_("Restart"));
					this._restart.connect('activate', () => Docker.run_command(Docker.docker_commands.c_restart, container));
					this.menu.addMenuItem(this._restart);
					break;

				case "paused":
					this._unpause = new PopupMenu.PopupMenuItem(_("Unpause"));
					this._unpause.connect('activate', () => Docker.run_command(Docker.docker_commands.c_unpause, container));
					this.menu.addMenuItem(this._unpause);

					this._stop = new PopupMenu.PopupMenuItem(_("Stop"));
					this._stop.connect('activate', () => Docker.run_command(Docker.docker_commands.c_stop, container));
					this.menu.addMenuItem(this._stop);

					this._restart = new PopupMenu.PopupMenuItem(_("Restart"));
					this._restart.connect('activate', () => Docker.run_command(Docker.docker_commands.c_restart, container));
					this.menu.addMenuItem(this._restart);
					break;

				default:
					this._start = new PopupMenu.PopupMenuItem(_("Start"));
					this._start.connect('activate', () => Docker.run_command(Docker.docker_commands.c_start, container));
					this.menu.addMenuItem(this._start);

					this._start_i = new PopupMenu.PopupMenuItem(_("Start interactive"));
					this._start_i.connect('activate', () => Docker.run_command(Docker.docker_commands.c_start_i, container));
					this.menu.addMenuItem(this._start_i);
			}

			this._logs = new PopupMenu.PopupMenuItem(_("View Logs"));
			this._logs.connect('activate', () => Docker.run_command(Docker.docker_commands.c_logs, container));
			this.menu.addMenuItem(this._logs);

			this._inspect = new PopupMenu.PopupMenuItem(_("Inspect"));
			this._inspect.connect('activate', () => Docker.run_command(Docker.docker_commands.c_inspect, container));
			this.menu.addMenuItem(this._inspect);

			// Remove
			this._remove = new PopupMenu.PopupMenuItem(Docker.docker_commands.c_rm.label);
			this._remove.connect(
				'activate',
				() => Confirm_Dialog.open(
					Docker.docker_commands.c_rm.label,
					`Are you sure you want to ${Docker.docker_commands.c_rm.label}?`,
					() => Docker.run_command(Docker.docker_commands.c_rm, container),
				)
			);
			this.menu.addMenuItem(this._remove);

			this.add_compose();
			this.add_ports();
		}
		add_compose() {
			// Check if container belongs to compose.
			if (!this._container.compose_dir) {
				return;
			}

			this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem(`Compose:${this._container.compose_project}`));

			switch (this._container.state) {
				case "running":
					// Stop
					this._compose_stop = new PopupMenu.PopupMenuItem(Docker.docker_commands.compose_stop.label);
					this._compose_stop.connect('activate', () => Docker.run_command(Docker.docker_commands.compose_stop, this._container));
					this.menu.addMenuItem(this._compose_stop);
					break;

				default:
					// Up
					this._compose_up = new PopupMenu.PopupMenuItem(Docker.docker_commands.compose_up.label);
					this._compose_up.connect('activate', () => Docker.run_command(Docker.docker_commands.compose_up, this._container));
					this.menu.addMenuItem(this._compose_up);

					// Remove
					this._compose_remove = new PopupMenu.PopupMenuItem(Docker.docker_commands.compose_rm.label);
					this._compose_remove.connect(
						'activate',
						() => Confirm_Dialog.open(
							Docker.docker_commands.compose_rm.label + this._compose_label,
							`Are you sure you want to ${Docker.docker_commands.compose_rm.label}?`,
							() => Docker.run_command(Docker.docker_commands.compose_rm, this._container),
						)
					);
					this.menu.addMenuItem(this._compose_remove);
					break;
			}
		}
	}
)