import Adw from 'gi://Adw';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk';

import { ExtensionPreferences, gettext as _ } from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

export default class DockerPreferences extends ExtensionPreferences {
	fillPreferencesWindow(window) {
		// Use the same GSettings schema as in `extension.js`
		this._settings = this.getSettings();

		const page = new Adw.PreferencesPage();
		window.add(page);


		// GROUP: Logo
		const group_logo = new Adw.PreferencesGroup();
		group_logo.set_title("Logo");
		page.add(group_logo);

		group_logo.add(this._logo());
		group_logo.add(this._up_container_timer());

		// GROUP: UI
		const group_ui = new Adw.PreferencesGroup();
		group_ui.set_title("UI");
		page.add(group_ui);
		
		group_ui.add(this._switch("show-information"));
		group_ui.add(this._menu_type());
		group_ui.add(this._visible_menus());
		group_ui.add(this._switch("show-ports"));
		group_ui.add(this._submenu_text_size());
		group_ui.add(this._submenu_image_size());
		
		// GROUP: Technical
		const group_technical = new Adw.PreferencesGroup();
		group_technical.set_title("Technical");
		page.add(group_technical);

		group_technical.add(this._terminal());
	}

	_logo() {
		const row_logo = new Adw.ActionRow({
			title: this._settings.settings_schema.get_key("logo").get_summary(),
			subtitle: this._settings.settings_schema.get_key("logo").get_description(),
		});

		const dropDown = Gtk.DropDown.new_from_strings([
			"Default",
			"Black",
			"Transparent",
			"White",
		]);
		dropDown.set_selected(this._settings.get_enum("logo"));
		dropDown.set_valign(Gtk.Align.CENTER);

		dropDown.connect('notify::selected', function() {
			let selected = dropDown.get_selected_item().get_string().toLowerCase();
			this._settings.set_string("logo", selected);
		}.bind(this));

		row_logo.add_suffix(dropDown);
		row_logo.activatable_widget = dropDown;

		return row_logo;
	}

	_up_container_timer() {
		const row = new Adw.ActionRow({
			title: this._settings.settings_schema.get_key("up-containers-timer").get_summary(),
			subtitle: this._settings.settings_schema.get_key("up-containers-timer").get_description(),
		});

		const spin = Gtk.SpinButton.new_with_range(0, 60, 5);
		spin.set_valign(Gtk.Align.CENTER);

		this._settings.bind(
			"up-containers-timer",
			spin,
			"value",
			Gio.SettingsBindFlags.DEFAULT
		);


		row.add_suffix(spin);
		row.set_activatable_widget(spin);

		return row
	}

	_menu_type() {
		const row_menu_type = new Adw.ActionRow({
			title: this._settings.settings_schema.get_key("menu-type").get_summary(),
			subtitle: this._settings.settings_schema.get_key("menu-type").get_description(),
		});

		const dropDown= Gtk.DropDown.new_from_strings([
			"Text",
			"Icons"
		]);
		dropDown.set_selected(this._settings.get_enum("menu-type"));
		dropDown.set_valign(Gtk.Align.CENTER);

		dropDown.connect('notify::selected', function() {
			let selected = dropDown.get_selected_item().get_string().toLowerCase();
			this._settings.set_string("menu-type", selected);
		}.bind(this));

		row_menu_type.add_suffix(dropDown);
		row_menu_type.activatable_widget = dropDown;

		return row_menu_type;
	}

	_visible_menus() {
		const row_visible_menu = new Adw.ActionRow({
			title: this._settings.settings_schema.get_key("show-value").get_summary(),
			subtitle: this._settings.settings_schema.get_key("show-value").get_description(),
		});

		const dropDown= Gtk.DropDown.new_from_strings([
			"Both",
			"Containers",
			"Images",
		]);
		dropDown.set_selected(this._settings.get_enum("show-value"));
		dropDown.set_valign(Gtk.Align.CENTER);

		dropDown.connect('notify::selected', function() {
			let selected = dropDown.get_selected_item().get_string().toLowerCase();
			this._settings.set_string("show-value", selected);
		}.bind(this));

		row_visible_menu.add_suffix(dropDown);
		row_visible_menu.activatable_widget = dropDown;

		return row_visible_menu;
	}

	_submenu_text_size() {
		const row_submenu_text_size = new Adw.ActionRow({
			title: this._settings.settings_schema.get_key("submenu-text").get_summary(),
			subtitle: this._settings.settings_schema.get_key("submenu-text").get_description(),
		});

		const spinButton_submenu_text_size = Gtk.SpinButton.new_with_range(1, 1000, 2)
		spinButton_submenu_text_size.set_valign(Gtk.Align.CENTER);

		this._settings.bind(
			'submenu-text',
			spinButton_submenu_text_size,
			'value',
			Gio.SettingsBindFlags.DEFAULT
		);


		row_submenu_text_size.add_suffix(spinButton_submenu_text_size);
		row_submenu_text_size.activatable_widget = spinButton_submenu_text_size;

		return row_submenu_text_size;
	}

	_submenu_image_size() {
		const row_submenu_img_size = new Adw.ActionRow({
			title: this._settings.settings_schema.get_key("submenu-image").get_summary(),
			subtitle: this._settings.settings_schema.get_key("submenu-image").get_description(),
		});

		const spinButton_submenu_img_size = Gtk.SpinButton.new_with_range(1, 1000, 2)
		spinButton_submenu_img_size.set_valign(Gtk.Align.CENTER);

		this._settings.bind(
			'submenu-image',
			spinButton_submenu_img_size,
			'value',
			Gio.SettingsBindFlags.DEFAULT
		);


		row_submenu_img_size.add_suffix(spinButton_submenu_img_size);
		row_submenu_img_size.activatable_widget = spinButton_submenu_img_size;

		return row_submenu_img_size;
	}

	_terminal() {
		const row_terminal = new Adw.ActionRow({
			title: this._settings.settings_schema.get_key("terminal").get_summary(),
			subtitle: this._settings.settings_schema.get_key("terminal").get_description(),
		});

		const input_terminal = new Gtk.Entry();
		input_terminal.set_valign(Gtk.Align.CENTER);

		this._settings.bind(
			'terminal',
			input_terminal,
			'text',
			Gio.SettingsBindFlags.DEFAULT
		);

		row_terminal.add_suffix(input_terminal);
		row_terminal.activatable_widget = input_terminal;

		return row_terminal;
	}

	/**
	 * Creates a row with a switch from
	 * the key schema
	 *
	 * @param {string} key - the key string from schema
	 * @returns {Adw.ActionRow}
	 */
	_switch(key) {
		const row = new Adw.ActionRow({
			title: this._settings.settings_schema.get_key(key).get_summary(),
			subtitle: this._settings.settings_schema.get_key(key).get_description(),
		});

		const gtk_switch = new Gtk.Switch();
		gtk_switch.set_valign(Gtk.Align.CENTER);

		this._settings.bind(
			key,
			gtk_switch,
			'state',
			Gio.SettingsBindFlags.DEFAULT
		);

		row.add_suffix(gtk_switch);
		row.set_activatable_widget(gtk_switch);

		return row
	}
}
